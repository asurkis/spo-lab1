#include "utils.h"
#include <stdlib.h>
#include <string.h>

string_list_t *string_list_init(char const *str, size_t len) {
  string_list_t *result = malloc(sizeof(string_list_t *) + len + 1);
  memcpy(result->data, str, len);
  result->data[len] = 0;
  return result;
}

void string_list_free(string_list_t *list) {
  if (list) {
    string_list_t *next = list->next;
    free(list);
    string_list_free(next);
  }
}

void string_list_append(string_list_t **where, char const *str, size_t len) {
  string_list_t *new_ = string_list_init(str, len);
  new_->next = *where;
  *where = new_;
}

void string_list_remove(string_list_t **where) {
  string_list_t *next = (*where)->next;
  (*where)->next = NULL;
  string_list_free(*where);
  *where = next;
}
